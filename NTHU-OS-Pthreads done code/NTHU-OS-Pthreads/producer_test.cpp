#include "ts_queue.hpp"
#include "reader.hpp"
#include "writer.hpp"
#include "producer.hpp"

int main()
{
	TSQueue<Item *> *q1;
	TSQueue<Item *> *q2;

	q1 = new TSQueue<Item *>;
	q2 = new TSQueue<Item *>;

	Transformer *transformer = new Transformer;

	Reader *reader = new Reader(80, "./tests/00.in", q1);
	std::cout << "reader done" << std::endl;
	Writer *writer = new Writer(80, "./tests/00.out", q2);
	std::cout << "writer done" << std::endl;

	Producer *p1 = new Producer(q1, q2, transformer);
	Producer *p2 = new Producer(q1, q2, transformer);
	Producer *p3 = new Producer(q1, q2, transformer);
	Producer *p4 = new Producer(q1, q2, transformer);
	std::cout << "Successfully new 4 producers." << std::endl;

	reader->start();
	writer->start();
	std::cout << "reader/writer Start!" << std::endl;
	p1->start();
	p2->start();
	p3->start();
	p4->start();
	std::cout << "p1~p4 Start!" << std::endl;
	reader->join();
	std::cout << "reader join!" << std::endl;
	writer->join();
	std::cout << "writer join!" << std::endl;
	delete p2;
	delete p1;
	delete writer;
	delete reader;
	delete q2;
	delete q1;

	return 0;
}
