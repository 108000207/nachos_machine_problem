#include <assert.h>
#include <stdlib.h>
#include "ts_queue.hpp"
#include "item.hpp"
#include "reader.hpp"
#include "writer.hpp"
#include "producer.hpp"
#include "consumer_controller.hpp"
#include <time.h>
#define READER_QUEUE_SIZE 200
#define WORKER_QUEUE_SIZE 200
#define WRITER_QUEUE_SIZE 4000
#define CONSUMER_CONTROLLER_LOW_THRESHOLD_PERCENTAGE 20
#define CONSUMER_CONTROLLER_HIGH_THRESHOLD_PERCENTAGE 80
#define CONSUMER_CONTROLLER_CHECK_PERIOD 1000000

int main(int argc, char **argv)
{
	assert(argc == 4);
	double START, END;
	START = clock();
	int n = atoi(argv[1]);
	std::string input_file_name(argv[2]);
	std::string output_file_name(argv[3]);

	// TODO: implements main function
	TSQueue<Item *> *q1 = new TSQueue<Item *>;
	TSQueue<Item *> *q2 = new TSQueue<Item *>;
	TSQueue<Item *> *q3 = new TSQueue<Item *>;

	Transformer *transformer = new Transformer;
	Reader *reader = new Reader(n, input_file_name, q1);
	Writer *writer = new Writer(n, output_file_name, q3);
	ConsumerController *ctrl = new ConsumerController(q2, q3, transformer, CONSUMER_CONTROLLER_CHECK_PERIOD, CONSUMER_CONTROLLER_LOW_THRESHOLD_PERCENTAGE, CONSUMER_CONTROLLER_HIGH_THRESHOLD_PERCENTAGE);
	Producer *p1 = new Producer(q1, q2, transformer);
	Producer *p2 = new Producer(q1, q2, transformer);
	Producer *p3 = new Producer(q1, q2, transformer);
	Producer *p4 = new Producer(q1, q2, transformer);

	// Consumer *p5 = new Consumer(q2, q3, transformer);
	// Consumer *p6 = new Consumer(q2, q3, transformer);
	// Consumer *p7 = new Consumer(q2, q3, transformer);
	// Consumer *p8 = new Consumer(q2, q3, transformer);
	// Consumer *p9 = new Consumer(q2, q3, transformer);
	// Consumer *p10 = new Consumer(q2, q3, transformer);
	// Consumer *p11 = new Consumer(q2, q3, transformer);
	// Consumer *p12 = new Consumer(q2, q3, transformer);
	// Consumer *p13 = new Consumer(q2, q3, transformer);
	// Consumer *p14 = new Consumer(q2, q3, transformer);

	reader->start();
	ctrl->start();
	writer->start();
	p1->start();
	p2->start();
	p3->start();
	p4->start();
	// p5->start();
	// p6->start();
	// p7->start();
	// p8->start();
	// p9->start();
	// p10->start();
	// p11->start();
	// p12->start();
	// p13->start();
	// p14->start();

	reader->join();
	writer->join();

	END = clock();
	// std::cout << "Time: " << (double)((END - START) / CLOCKS_PER_SEC) << std::endl;

	delete writer;
	delete reader;
	return 0;
}
