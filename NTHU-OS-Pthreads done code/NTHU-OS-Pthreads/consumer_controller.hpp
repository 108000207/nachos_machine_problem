#include <pthread.h>
#include <unistd.h>
#include <vector>
#include <iostream>
#include "consumer.hpp"
#include "ts_queue.hpp"
#include "item.hpp"
#include "transformer.hpp"
// #include "main.cpp"


#define READER_QUEUE_SIZE 200
#define WORKER_QUEUE_SIZE 200
#define WRITER_QUEUE_SIZE 4000
#define CONSUMER_CONTROLLER_LOW_THRESHOLD_PERCENTAGE 20
#define CONSUMER_CONTROLLER_HIGH_THRESHOLD_PERCENTAGE 80
#define CONSUMER_CONTROLLER_CHECK_PERIOD 1000000


#ifndef CONSUMER_CONTROLLER
#define CONSUMER_CONTROLLER

class ConsumerController : public Thread
{
public:
	// constructor
	ConsumerController(
		TSQueue<Item *> *worker_queue,
		TSQueue<Item *> *writer_queue,
		Transformer *transformer,
		int check_period,
		int low_threshold,
		int high_threshold);

	// destructor
	~ConsumerController();

	virtual void start();

private:
	std::vector<Consumer *> consumers;

	TSQueue<Item *> *worker_queue;
	TSQueue<Item *> *writer_queue;

	Transformer *transformer;

	// Check to scale down or scale up every check period in microseconds.
	int check_period;
	// When the number of items in the worker queue is lower than low_threshold,
	// the number of consumers scaled down by 1.
	int low_threshold;
	// When the number of items in the worker queue is higher than high_threshold,
	// the number of consumers scaled up by 1.
	int high_threshold;

	static void *process(void *arg);
};

// Implementation start

ConsumerController::ConsumerController(
	TSQueue<Item *> *worker_queue,
	TSQueue<Item *> *writer_queue,
	Transformer *transformer,
	int check_period,
	int low_threshold,
	int high_threshold) : worker_queue(worker_queue),
						  writer_queue(writer_queue),
						  transformer(transformer),
						  check_period(check_period),
						  low_threshold(low_threshold),
						  high_threshold(high_threshold)
{
}

ConsumerController::~ConsumerController() {}

void ConsumerController::start()
{
	// TODO: starts a ConsumerController thread
	pthread_create(&t, 0, ConsumerController::process, (void *)this);
}

void *ConsumerController::process(void *arg)
{
	ConsumerController *ctrl = (ConsumerController *)arg;
	// ctrl->check_period = CONSUMER_CONTROLLER_CHECK_PERIOD;
	// ctrl->low_threshold=
	// ctrl->high_threshold=

	// TODO: implements the ConsumerController's work
	while (true)
	{
		usleep(ctrl->check_period);
		// std::cout << "in ConsumerController peroid\n";
		int workerQ_ratio = ctrl->worker_queue->get_size() * 100 / WORKER_QUEUE_SIZE;
		// std::cout << "---ctrl---: " << workerQ_ratio << " : " << ctrl->low_threshold << std::endl;
		if (workerQ_ratio > ctrl->high_threshold) // workerQ_ratio > 80
		{
			// std::cout << "flag0\n";
			Consumer *newConsumer = new Consumer(ctrl->worker_queue, ctrl->writer_queue, ctrl->transformer);
			// std::cout << "flag1\n";
			newConsumer->start();
			// std::cout << "flag2\n";
			ctrl->consumers.push_back(newConsumer);
			std::cout << "Scaling up consumers from " << ctrl->consumers.size() - 1 << " to " << ctrl->consumers.size() << std::endl;
		}
		else if ((workerQ_ratio < ctrl->low_threshold) && (ctrl->consumers.size() > 1)) // workerQ_ratio < 20
		{
			// std::cout << "flag3\n";
			Consumer *deleteConsumer = ctrl->consumers.back();
			// std::cout << "flag4\n";
			ctrl->consumers.pop_back();
			// std::cout << "flag5\n";
			int temp = deleteConsumer->cancel();
			std::cout << "Scaling down consumers from " << ctrl->consumers.size() + 1 << " to " << ctrl->consumers.size() << std::endl;
		}
	}
}

#endif // CONSUMER_CONTROLLER_HPP
