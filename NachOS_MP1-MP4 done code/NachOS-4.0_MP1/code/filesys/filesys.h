// filesys.h
//	Data structures to represent the Nachos file system.
//
//	A file system is a set of files stored on disk, organized
//	into directories.  Operations on the file system have to
//	do with "naming" -- creating, opening, and deleting files,
//	given a textual file name.  Operations on an individual
//	"open" file (read, write, close) are to be found in the OpenFile
//	class (openfile.h).
//
//	We define two separate implementations of the file system.
//	The "STUB" version just re-defines the Nachos file system
//	operations as operations on the native UNIX file system on the machine
//	running the Nachos simulation.
//
//	The other version is a "real" file system, built on top of
//	a disk simulator.  The disk is simulated using the native UNIX
//	file system (in a file named "DISK").
//
//	In the "real" implementation, there are two key data structures used
//	in the file system.  There is a single "root" directory, listing
//	all of the files in the file system; unlike UNIX, the baseline
//	system does not provide a hierarchical directory structure.
//	In addition, there is a bitmap for allocating
//	disk sectors.  Both the root directory and the bitmap are themselves
//	stored as files in the Nachos file system -- this causes an interesting
//	bootstrap problem when the simulated disk is initialized.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation
// of liability and disclaimer of warranty provisions.

#ifndef FS_H
#define FS_H

#include "copyright.h"
#include "sysdep.h"
#include "openfile.h"
#include "debug.h" //just for test!!!

#define NumDirEntries 10
#define OpenFileTableSize 20
#include "directory.h"

/* #ifdef FILESYS_STUB // Temporarily implement file system calls as */
// calls to UNIX, until the real file system
// implementation is available
typedef int OpenFileId;

class FileSystem
{
public:
    FileSystem()
    {
        for (int i = 0; i < OpenFileTableSize; i++)
            OpenFileTable[i] = NULL;
    }

    bool Create(char *name)
    {
        int fileDescriptor = OpenForWrite(name);

        if (fileDescriptor == -1)
            return FALSE;
        Close(fileDescriptor);
        return TRUE;
    }

    /* 
        Requirements
        (a). Must maintain OpenFileTable and use the table entry number of
        OpenFileTable as the OpenFileId.
        (b). Must use OpenFile object to access file. Do Not directly use the function in
        “sysdep.h” to access file.
        (c). Must handle invalid file open requests, including the non-existent file,
        exceeding opened file limit (at most 20 files), etc.
        (d). All valid file open requests must be accepted if the opened file limit (at most 20
        files) is not reached.
        (e). Must handle invalid file read, write, close requests, including invalid id, etc.
        (f). DO NOT use any IO functions from standard libraries (e.g. printf(), cout, fopen(),
        fwrite(), write(), etc.).
        (g). DO NOT change any code under “machine/” folder
        (h). DO NOT modify the content of OpenFileTable outside “filesystem/” folder
        (i). DO NOT modify the declaration of OpenFileTable, including the size.
        */

    //The OpenFile function is used for open user program  [userprog/addrspace.cc]
    OpenFile *Open(char *name)
    {
        int fileDescriptor = OpenForReadWrite(name, FALSE);
        if (fileDescriptor == -1)
            return NULL;
        return new OpenFile(fileDescriptor);
    }

    //  The OpenAFile function is used for kernel open system call
    OpenFileId OpenAFile(char *name)
    {
        // TODO5
        // Open a file with the name, and return its corresponding OpenFileId.
        // Return -1 if fail to open the file.
        // DEBUG(dbgSys, "Enter OpenAFile ......\n");
        OpenFile *id = Open(name);
        if (id == NULL)
            return -1;
        else
        {
            for (int i = 0; i < OpenFileTableSize; i++)
            {
                if (OpenFileTable[i] == NULL)
                {
                    OpenFileTable[i] = id;
                    // DEBUG(dbgSys, "Find a file" << name << "where its id is" << i << "\n");
                    return i;
                }
            }

            return -1;
        }
    }
    int WriteFile(char *buffer, int size, OpenFileId id)
    {
        // TODO6
        // Write “size” characters from the buffer into the file, and return the number of
        // characters actually written to the file.
        // Return -1, if fail to write the file.

        // DEBUG(dbgSys, "filesys.h: Enter WriteFile ...... id: " << id << "\n");
        if (id >= 0 && id < 20)
        {
            OpenFile *FileToWrite = OpenFileTable[id];
            int status = FileToWrite->Write(buffer, size);
            // DEBUG(dbgSys, "filesys.h: WriteFile status:" << status << "\n");
            if (status != 0)
                return status;
        }
        return -1;
    }
    int ReadFile(char *buffer, int size, OpenFileId id)
    {
        // TODO7
        // Read “size” characters from the file to the buffer, and return the number of
        // characters actually read from the file.
        // Return -1, if fail to read the file.
        // DEBUG(dbgSys, "Enter Read File ...... id: " << id << "\n");
        if (id >= 0 && id < 20)
        {
            OpenFile *FileToRead = OpenFileTable[id];
            int status = FileToRead->Read(buffer, size);
            // DEBUG(dbgSys, "Read File status:" << status << "\n");
            if (status != 0)
                return status;
        }

        return -1;
    }
    int CloseFile(OpenFileId id)
    {
        // TODO8
        // Close the file with id.
        // Return 1 if successfully close the file. Otherwise, return -1.
        // Need to delete the OpenFile after you close the file
        // (Can’t only set the table content to NULL)

        // DEBUG(dbgSys, "filesys.h: Close File id: " << id << "\n");
        if (id >= 0 && id < 20 && OpenFileTable[id] != NULL)
        {
            delete OpenFileTable[id];
            OpenFileTable[id] = NULL;
            return 1;
        }
        return -1;
    }

    bool Remove(char *name) { return Unlink(name) == 0; }

    OpenFile *OpenFileTable[OpenFileTableSize];
};

/* #else // FILESYS


class FileSystem
{
public:
    FileSystem(bool format); // Initialize the file system.
                             // Must be called *after* "synchDisk"
                             // has been initialized.
                             // If "format", there is nothing on
                             // the disk, so initialize the directory
                             // and the bitmap of free blocks.

    bool Create(char *name, int initialSize);
    // Create a file (UNIX creat)

    OpenFile *Open(char *name); // Open a file (UNIX open)

    bool Remove(char *name); // Delete a file (UNIX unlink)

    void List(); // List all the files in the file system

    void Print(); // List all the files and their contents

private:
    OpenFile *freeMapFile;   // Bit map of free disk blocks,
                             // represented as a file
    OpenFile *directoryFile; // "Root" directory -- list of
                             // file names, represented as a file
};

#endif // FILESYS
 */
#endif // FS_H
