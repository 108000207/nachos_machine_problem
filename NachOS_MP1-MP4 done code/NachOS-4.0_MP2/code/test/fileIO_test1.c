#include "syscall.h"

int main(void)
{
	// int success10;
	// int success3;
	// OpenFileId fid10;
	// OpenFileId fid3;
	OpenFileId fid;
	int i;
	char test[] = "abcdefghijklmnopqrstuvwxyz";

	int success = Create("file1.test");
	if (success != 1)
		MSG("Failed on creating file");

	fid = Open("file1.test");
	if (fid < 0)
		MSG("Failed on opening file1");
	/* 
	fid10 = Open("file10.test");
	if (fid10 < 0)
		MSG("Failed on opening file10");

	success10 = Create("file10.test");
	if (success10 != 1)
		MSG("Failed on creating file10");

	fid10 = Open("file10.test");
	if (fid10 < 0)
		MSG("Failed on opening file10"); */

	// success3 = Create("file3.test");
	// if (success3 != 1)
	// 	MSG("Failed on creating file3");
	// fid3 = Open("file3.test");

	// if (fid < 0)
	// 	MSG("Failed on opening file1");
	// if (fid2 < 0)
	// 	MSG("Failed on opening file2");
	// if (fid3 < 0)
	// 	MSG("Failed on opening file3");

	for (i = 0; i < 26; ++i)
	{
		int count = Write(test + i, 1, fid);
		if (count != 1)
			MSG("Failed on writing file");
	}

	success = Close(fid);
	if (success != 1)
		MSG("Failed on closing file");
	MSG("Success on creating file1.test");
	Halt();
}
