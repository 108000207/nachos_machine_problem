// filesys.cc
//	Routines to manage the overall operation of the file system.
//	Implements routines to map from textual file names to files.
//
//	Each file in the file system has:
//	   A file header, stored in a sector on disk
//		(the size of the file header data structure is arranged
//		to be precisely the size of 1 disk sector)
//	   A number of data blocks
//	   An entry in the file system directory
//
// 	The file system consists of several data structures:
//	   A bitmap of free disk sectors (cf. bitmap.h)
//	   A directory of file names and file headers
//
//      Both the bitmap and the directory are represented as normal
//	files.  Their file headers are located in specific sectors
//	(sector 0 and sector 1), so that the file system can find them
//	on bootup.
//
//	The file system assumes that the bitmap and directory files are
//	kept "open" continuously while Nachos is running.
//
//	For those operations (such as Create, Remove) that modify the
//	directory and/or bitmap, if the operation succeeds, the changes
//	are written immediately back to disk (the two files are kept
//	open during all this time).  If the operation fails, and we have
//	modified part of the directory and/or bitmap, we simply discard
//	the changed version, without writing it back to disk.
//
// 	Our implementation at this point has the following restrictions:
//
//	   there is no synchronization for concurrent accesses
//	   files have a fixed size, set when the file is created
//	   files cannot be bigger than about 3KB in size
//	   there is no hierarchical directory structure, and only a limited
//	     number of files can be added to the system
//	   there is no attempt to make the system robust to failures
//	    (if Nachos exits in the middle of an operation that modifies
//	    the file system, it may corrupt the disk)
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation
// of liability and disclaimer of warranty provisions.
#ifndef FILESYS_STUB

#include "copyright.h"
#include "debug.h"
#include "disk.h"
#include "pbitmap.h"
#include "directory.h"
#include "filehdr.h"
#include "filesys.h"

// Sectors containing the file headers for the bitmap of free sectors,
// and the directory of files.  These file headers are placed in well-known
// sectors, so that they can be located on boot-up.
#define FreeMapSector 0
#define DirectorySector 1

// Initial file sizes for the bitmap and directory; until the file system
// supports extensible files, the directory size sets the maximum number
// of files that can be loaded onto the disk.
#define FreeMapFileSize (NumSectors / BitsInByte)                  // 128 bytes
#define DirectoryFileSize (sizeof(DirectoryEntry) * NumDirEntries) // 200 bytes

//----------------------------------------------------------------------
// FileSystem::FileSystem
// 	Initialize the file system.  If format = TRUE, the disk has
//	nothing on it, and we need to initialize the disk to contain
//	an empty directory, and a bitmap of free sectors (with almost but
//	not all of the sectors marked as free).
//
//	If format = FALSE, we just have to open the files
//	representing the bitmap and the directory.
//
//	"format" -- should we initialize the disk?
//----------------------------------------------------------------------

FileSystem::FileSystem(bool format)
{
    DEBUG(dbgFile, "Initializing the file system.");
    if (format)
    {
        PersistentBitmap *freeMap = new PersistentBitmap(NumSectors);
        Directory *directory = new Directory(NumDirEntries);
        FileHeader *mapHdr = new FileHeader;
        FileHeader *dirHdr = new FileHeader;

        DEBUG(dbgFile, "Formatting the file system.");
        // First, allocate space for FileHeaders for the directory and bitmap
        // (make sure no one else grabs these!)
        freeMap->Mark(FreeMapSector); // !! FreeMapSector is used for mapHdr
        freeMap->Mark(DirectorySector);
        // Second, allocate space for the data blocks containing the contents
        // of the directory and bitmap files.  There better be enough space!

        ASSERT(mapHdr->Allocate(freeMap, FreeMapFileSize));
        ASSERT(dirHdr->Allocate(freeMap, DirectoryFileSize));
        // Flush the bitmap and directory FileHeaders back to disk
        // We need to do this before we can "Open" the file, since open
        // reads the file header off of disk (and currently the disk has garbage
        // on it!).

        DEBUG(dbgFile, "Writing headers back to disk.");
        mapHdr->WriteBack(FreeMapSector);
        dirHdr->WriteBack(DirectorySector);

        // OK to open the bitmap and directory files now
        // The file system operations assume these two files are left open
        // while Nachos is running.
        // freeMapFile, mapHdr, freeMap
        freeMapFile = new OpenFile(FreeMapSector);
        directoryFile = new OpenFile(DirectorySector);

        // Once we have the files "open", we can write the initial version
        // of each file back to disk.  The directory at this point is completely
        // empty; but the bitmap has been changed to reflect the fact that
        // sectors on the disk have been allocated for the file headers and
        // to hold the file data for the directory and bitmap.

        DEBUG(dbgFile, "Writing bitmap and directory back to disk.");
        freeMap->WriteBack(freeMapFile); // flush changes to disk
        directory->WriteBack(directoryFile);
        // DEBUG(dbgFile, );
        if (debug->IsEnabled('f'))
        {
            freeMap->Print();
            directory->Print();
        }
        delete freeMap;
        delete directory;
        delete mapHdr;
        delete dirHdr;
    }
    else
    {
        // if we are not formatting the disk, just open the files representing
        // the bitmap and directory; these are left open while Nachos is running
        freeMapFile = new OpenFile(FreeMapSector);
        directoryFile = new OpenFile(DirectorySector);
    }
}

//----------------------------------------------------------------------
// MP4 mod tag
// FileSystem::~FileSystem
//----------------------------------------------------------------------
FileSystem::~FileSystem()
{
    delete freeMapFile;
    delete directoryFile;
}

//----------------------------------------------------------------------
// FileSystem::Create
// 	Create a file in the Nachos file system (similar to UNIX create).
//	Since we can't increase the size of files dynamically, we have
//	to give Create the initial size of the file.
//
//	The steps to create a file are:
//	  Make sure the file doesn't already exist
//        Allocate a sector for the file header
// 	  Allocate space on disk for the data blocks for the file
//	  Add the name to the directory
//	  Store the new file header on disk
//	  Flush the changes to the bitmap and the directory back to disk
//
//	Return TRUE if everything goes ok, otherwise, return FALSE.
//
// 	Create fails if:
//   		file is already in directory
//	 	no free space for file header
//	 	no free entry for file in directory
//	 	no free space for data blocks for the file
//
// 	Note that this implementation assumes there is no concurrent access
//	to the file system!
//
//	"name" -- name of file to be created
//	"initialSize" -- size of file to be created
//----------------------------------------------------------------------

char *GetFileName(char *fullpath)
{
    char *filename;
    filename = strrchr(fullpath, '/');
    filename++;
    return filename;
}
pair<Directory *, char *> FileSystem::getNameAndParentDir(Directory *directory, char *name)
{
    int sector;
    OpenFile *file_tem = directoryFile;
    char *token = strtok(name, "/"); // /t0 or name
    char *token_prev;
    token_prev = token; // /t0 or name

    while (token != NULL)
    {
        if ((sector = directory->Find(token)) == -1)
            break;
        printf("current directory name = %s\n", token);
        file_tem = new OpenFile(sector);
        directory->FetchFrom(file_tem);
        token_prev = token;
        token = strtok(NULL, "/"); // /t1 or name
    }
    // name = token;
    printf("file name = %s\n", name);
    return make_pair(directory, token);
}
bool FileSystem::Create(char *name, int initialSize)
{
    Directory *directory;
    PersistentBitmap *freeMap;
    FileHeader *hdr;
    int sector;
    bool success;

    DEBUG(dbgFile, "Creating file " << name << " size " << initialSize);

    directory = new Directory(NumDirEntries);
    directory->FetchFrom(directoryFile); // for now it is root

    //-------------------------------------------
    char dirPath[255][10];
    char *filename = GetFileName(name);
    int layer = 0;
    int fullpath_len = strlen(name) + 1;
    char *fullpath_tmp = new char[fullpath_len]();
    memcpy(fullpath_tmp, name, fullpath_len);
    char *dirname = strtok(name, "/"); // aaa

    while (dirname != filename)
    {
        memcpy(dirPath[layer], dirname, strlen(dirname) + 1);
        layer++;
        dirname = strtok(NULL, "/");
    }
    memcpy(name, fullpath_tmp, fullpath_len);
    delete fullpath_tmp;
    //--------------------parse------------------
    OpenFile *openFile = NULL;
    int parentSector = 1;
    DEBUG(dbgFile, "this is layer: " << layer);
    for (int i = 0; i < layer; i++)
    {
        parentSector = directory->Find(dirPath[i]);
        DEBUG(dbgFile, "this is dirPath: " << dirPath[i] << " : " << parentSector);

        // DEBUG(dbgFile, "---MP4--- Return from Find table, parentSector: "<< parentSector);
        if (parentSector >= 0)
        {
            openFile = new OpenFile(parentSector); // name was found in directory
        }
        if (openFile != NULL)
            directory->FetchFrom(openFile);
    }
    // -------------------find parent-------------------------
    // pair<Directory *, char *> my_pair = getNameAndParentDir(directory, name);
    // directory = my_pair.first;
    // filename = my_pair.second;

    if (directory->Find(filename) != -1)
    {
        DEBUG(dbgFile, "Find already in directory!!");
        success = FALSE; // file is already in directory
    }
    else
    {
        freeMap = new PersistentBitmap(freeMapFile, NumSectors);
        sector = freeMap->FindAndSet(); // find a sector to hold the file header
        DEBUG(dbgFile, "Create File Name: " << filename << " : " << sector);
        if (sector == -1)
            success = FALSE; // no free block for file header
        else if (!directory->Add(filename, sector, 'F'))
            success = FALSE; // no space in directory
        else
        {
            hdr = new FileHeader;
            if (!hdr->Allocate(freeMap, initialSize))
                success = FALSE; // no space on disk for data
            else
            {
                success = TRUE;
                // everthing worked, flush all changes back to disk
                hdr->WriteBack(sector);
                if (!openFile)
                    openFile = new OpenFile(parentSector); // name was found in directory
                directory->WriteBack(openFile);
                // directory->WriteBack(directoryFile);
                freeMap->WriteBack(freeMapFile);
            }
            delete hdr;
        }
        delete freeMap;
    }
    delete directory;
    return success;
}

//----------------------------------------------------------------------
// FileSystem::Open
// 	Open a file for reading and writing.
//	To open a file:
//	  Find the location of the file's header, using the directory
//	  Bring the header into memory
//
//	"name" -- the text name of the file to be opened
//----------------------------------------------------------------------

OpenFile *FileSystem::Open(char *name)
{
    Directory *directory = new Directory(NumDirEntries);
    OpenFile *openFile = NULL;
    int sector;

    //-------------------------------------------
    char dirPath[255][10];
    char *filename = GetFileName(name);
    int layer = 0;
    int fullpath_len = strlen(name) + 1;
    char *fullpath_tmp = new char[fullpath_len]();
    memcpy(fullpath_tmp, name, fullpath_len);
    char *dirname = strtok(name, "/"); // aaa

    while (dirname != filename)
    {
        memcpy(dirPath[layer], dirname, strlen(dirname) + 1);
        layer++;
        dirname = strtok(NULL, "/");
    }
    memcpy(name, fullpath_tmp, fullpath_len);
    delete fullpath_tmp;
    //--------------------parse------------------
    int parentSector = 1; // inital as root Directory
    directory->FetchFrom(directoryFile);
    for (int i = 0; i < layer; i++)
    {
        parentSector = directory->Find(dirPath[i]);
        // DEBUG(dbgFile, "---MP4--- Return from Find table, parentSector: "<< parentSector);
        if (parentSector >= 0)
        {
            openFile = new OpenFile(parentSector); // name was found in directory
        }
        if (openFile != NULL)
            directory->FetchFrom(openFile);
    }
    // -------------------find parent-------------------------
    DEBUG(dbgFile, "Opening file " << filename);
    // directory->FetchFrom(directoryFile);
    sector = directory->Find(filename);
    // DEBUG(dbgFile, "---MP4--- Return from Find table, sector: "<< sector);
    if (sector >= 0)
        openFile = new OpenFile(sector); // name was found in directory
    DEBUG(dbgFile, "In Open: parentSector: " << parentSector << ", Sector: " << sector);
    delete directory;
    return openFile; // return NULL if not found
}

//----------------------------------------------------------------------
// FileSystem::Remove
// 	Delete a file from the file system.  This requires:
//	    Remove it from the directory
//	    Delete the space for its header
//	    Delete the space for its data blocks
//	    Write changes to directory, bitmap back to disk
//
//	Return TRUE if the file was deleted, FALSE if the file wasn't
//	in the file system.
//
//	"name" -- the text name of the file to be removed
//----------------------------------------------------------------------

bool FileSystem::Remove(char *name)
{
    Directory *directory;
    PersistentBitmap *freeMap;
    FileHeader *fileHdr;
    int sector;

    directory = new Directory(NumDirEntries);
    directory->FetchFrom(directoryFile);

    //-------------------------------------------
    char dirPath[255][10];
    char *filename = GetFileName(name);
    int layer = 0;
    int fullpath_len = strlen(name) + 1;
    char *fullpath_tmp = new char[fullpath_len]();
    memcpy(fullpath_tmp, name, fullpath_len);
    char *dirname = strtok(name, "/"); // aaa

    while (dirname != filename)
    {
        memcpy(dirPath[layer], dirname, strlen(dirname) + 1);
        layer++;
        dirname = strtok(NULL, "/");
    }
    memcpy(name, fullpath_tmp, fullpath_len);
    delete fullpath_tmp;
    //--------------------parse------------------
    OpenFile *openFile = NULL;
    int parentSector = 1;

    for (int i = 0; i < layer; i++)
    {
        parentSector = directory->Find(dirPath[i]);
        // DEBUG(dbgFile, "---MP4--- Return from Find table, parentSector: "<< parentSector);
        if (parentSector >= 0)
        {
            openFile = new OpenFile(parentSector); // name was found in directory
        }
        if (openFile != NULL)
            directory->FetchFrom(openFile);
    }
    // -------------------find parent-------------------------

    sector = directory->Find(filename);
    DEBUG(dbgFile, "delete file sector: " << sector);
    if (sector == -1)
    {
        delete directory;
        return FALSE; // file not found
    }
    fileHdr = new FileHeader;
    fileHdr->FetchFrom(sector);

    freeMap = new PersistentBitmap(freeMapFile, NumSectors);

    fileHdr->Deallocate(freeMap); // remove data blocks
    freeMap->Clear(sector);       // remove header block

    directory->Remove(filename);
    freeMap->WriteBack(freeMapFile); // flush to disk
    // directory->WriteBack(directoryFile); // flush to disk
    if (!openFile)
        openFile = new OpenFile(parentSector); // name was found in directory
    directory->WriteBack(openFile);

    DEBUG(dbgFile, "delete file done!");

    delete fileHdr;
    delete directory;
    delete freeMap;
    return TRUE;
}

//----------------------------------------------------------------------
// FileSystem::List
// 	List all the files in the file system directory.
//----------------------------------------------------------------------
void FileSystem::List(char *name, bool recursive)
{
    Directory *directory = new Directory(NumDirEntries);
    directory->FetchFrom(directoryFile); // for now it is root
    //-------------------------------------------
    if (strlen(name) != 1)
    {
        DEBUG(dbgFile, "-l input name != : " << name);
        char dirPath[255][10];
        char *filename = GetFileName(name);
        int layer = 0;
        int fullpath_len = strlen(name) + 1;
        char *fullpath_tmp = new char[fullpath_len]();
        memcpy(fullpath_tmp, name, fullpath_len);
        char *dirname = strtok(name, "/"); // d1

        while (dirname != filename)
        {
            memcpy(dirPath[layer], dirname, strlen(dirname) + 1);
            layer++;
            dirname = strtok(NULL, "/");
        }
        memcpy(dirPath[layer], filename, strlen(dirname) + 1);
        memcpy(name, fullpath_tmp, fullpath_len);
        delete fullpath_tmp;
        //--------------------parse------------------
        OpenFile *openFile = NULL;
        int currentSector = 1;
        DEBUG(dbgFile, "this is layer: " << layer);
        for (int i = 0; i <= layer; i++)
        {
            currentSector = directory->Find(dirPath[i]);
            DEBUG(dbgFile, "this is dirPath: " << dirPath[i] << " : " << currentSector);

            // DEBUG(dbgFile, "---MP4--- Return from Find table, currentSector: "<< currentSector);
            if (currentSector >= 0)
            {
                openFile = new OpenFile(currentSector); // name was found in directory
            }
            if (openFile != NULL)
            {
                directory->FetchFrom(openFile);
                delete openFile;
            }
        }
        // -------------------find parent-------------------------
    }
    else
    {
        DEBUG(dbgFile, "-l input name: " << name);
    }
    if (!recursive)
        directory->List(0);
    else
        directory->List(1);

    delete directory;
}

//----------------------------------------------------------------------
// FileSystem::Print
// 	Print everything about the file system:
//	  the contents of the bitmap
//	  the contents of the directory
//	  for each file in the directory,
//	      the contents of the file header
//	      the data in the file
//----------------------------------------------------------------------

void FileSystem::Print()
{
    FileHeader *bitHdr = new FileHeader;
    FileHeader *dirHdr = new FileHeader;
    PersistentBitmap *freeMap = new PersistentBitmap(freeMapFile, NumSectors);
    Directory *directory = new Directory(NumDirEntries);

    printf("Bit map file header:\n");
    bitHdr->FetchFrom(FreeMapSector);
    bitHdr->Print();

    printf("Directory file header:\n");
    dirHdr->FetchFrom(DirectorySector);
    dirHdr->Print();

    freeMap->Print();

    directory->FetchFrom(directoryFile);
    directory->Print();

    delete bitHdr;
    delete dirHdr;
    delete freeMap;
    delete directory;
}

// TODO !!!
int FileSystem::Create_MP4(char *name, int initialSize)
{
    if (Create(name, initialSize))
        return 1;
    else
        return 0;
    // return Create(name, initialSize);
}
OpenFileId FileSystem::Open_MP4(char *name)
{
    // DEBUG(dbgFile, "Call Open_MP4!!!!!!");
    OpenFile *openPtr = Open(name);
    // TODO: maybe need to use a variable to store the openfile
    if (openPtr != NULL)
    {
        // DEBUG(dbgFile, "---MP4---Call Open_MP4 open success");
        openFilePtr = openPtr;
        return 1;
    }
    else
    {
        // DEBUG(dbgFile, "---MP4---Call Open_MP4 open Fail");
        openFilePtr = NULL;
        return 0;
    }
}
int FileSystem::Read(char *buf, int size, OpenFileId id)
{
    if (openFilePtr != NULL)
    {
        // DEBUG(dbgFile, "---MP4---B: Ready To Read");
        return openFilePtr->Read(buf, size);
    }
    else
    {
        // DEBUG(dbgFile, "---MP4---C: Ready To Read But openFilePtr == NULL");
        return -1;
    }
}
int FileSystem::Write(char *buf, int size, OpenFileId id)
{
    if (openFilePtr != NULL)
        return openFilePtr->Write(buf, size);
    else
        return -1;
}
int FileSystem::Close(OpenFileId id)
{
    if (openFilePtr != NULL)
    {
        delete openFilePtr;
        openFilePtr = NULL;
        return 1;
    }
    else
        return -1;
}

void FileSystem::CreateDirectory(char *fullpath)
{
    // MP4 Assignment
    // TODO 5: parse
    // TODO 6: use a var to record parent
    // TODO 6-2: pointer 指向正確parent位置
    // TODO 6-3: new directory
    // TODO 7: find
    // TODO 8: add

    PersistentBitmap *freeMap;
    Directory *directory = new Directory(NumDirEntries);
    freeMap = new PersistentBitmap(freeMapFile, NumSectors);
    directory->FetchFrom(directoryFile);
    // ----------------------------------------------
    char dirPath[255][10];
    char *filename = GetFileName(fullpath);
    int layer = 0;
    int fullpath_len = strlen(fullpath) + 1;
    char *fullpath_tmp = new char[fullpath_len]();
    memcpy(fullpath_tmp, fullpath, fullpath_len);
    char *dirname = strtok(fullpath, "/"); // aaa

    while (dirname != filename)
    { // purpose is to get name before "filename"
      /// aaa/b/c/filename  => dirname == a/b/c/
      // aaa/b/c/filename  => dirname == a\0b\0c\0
        memcpy(dirPath[layer], dirname, strlen(dirname) + 1);
        layer++;
        dirname = strtok(NULL, "/");
    }
    memcpy(fullpath, fullpath_tmp, fullpath_len);
    delete fullpath_tmp;
    //--------------------parse------------------

    OpenFile *openFile = NULL;
    int parentSector = 1;
    for (int i = 0; i < layer; i++)
    {
        parentSector = directory->Find(dirPath[i]);
        // DEBUG(dbgFile, "---MP4--- Return from Find table, parentSector: "<< parentSector);
        if (parentSector >= 0)
        {
            openFile = new OpenFile(parentSector); // name was found in directory
        }
        if (openFile != NULL)
            directory->FetchFrom(openFile);
    }
    // -------------------find parent-------------------------
    int newSector = freeMap->FindAndSet();
    DEBUG(dbgFile, "---MP4---After FindAndSet: " << filename << " : " << newSector);
    directory->Add(filename, newSector, 'D');

    FileHeader *dirHdr = new FileHeader;
    ASSERT(dirHdr->Allocate(freeMap, DirectoryFileSize));
    dirHdr->WriteBack(newSector);

    if (!openFile)
        openFile = new OpenFile(parentSector); // name was found in directory
    directory->WriteBack(openFile);
    Directory *newDirectory = new Directory(NumDirEntries);
    OpenFile *newDirHdr = new OpenFile(newSector);
    // newDirectory->FetchFrom(newDirHdr);
    newDirectory->WriteBack(newDirHdr);

    freeMap->WriteBack(freeMapFile); // flush changes to disk
    // directory->WriteBack(directoryFile); // flush to disk

    if (debug->IsEnabled('f'))
    {
        freeMap->Print();
        // directory->Print();
    }
    delete newDirectory;
    delete newDirHdr;
    delete directory;
    delete dirHdr;
    delete openFile;
}

#endif // FILESYS_STUB