// scheduler.cc
//	Routines to choose the next thread to run, and to dispatch to
//	that thread.
//
// 	These routines assume that interrupts are already disabled.
//	If interrupts are disabled, we can assume mutual exclusion
//	(since we are on a uniprocessor).
//
// 	NOTE: We can't use Locks to provide mutual exclusion here, since
// 	if we needed to wait for a lock, and the lock was busy, we would
//	end up calling FindNextToRun(), and that would put us in an
//	infinite loop.
//
// 	Very simple implementation -- no priorities, straight FIFO.
//	Might need to be improved in later assignments.
//
// Copyright (c) 1992-1996 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "debug.h"
#include "scheduler.h"
#include "main.h"

static int comp1(Thread *t1, Thread *t2)
{
    if (t1->guessTime > t2->guessTime)
        return 1;
    else if (t1->guessTime < t2->guessTime)
        return -1;
    else
        return 0;
    // return t1->guessTime > t2->guessTime;
}
static int comp2(Thread *t1, Thread *t2)
{
    if (t1->priority > t2->priority)
        return -1;
    else if (t1->priority < t2->priority)
        return 1;
    else
        return 0;
}

//----------------------------------------------------------------------
// Scheduler::Scheduler
// 	Initialize the list of ready but not running threads.
//	Initially, no ready threads.
//----------------------------------------------------------------------

Scheduler::Scheduler()
{
    // readyList = new List<Thread *>;
    L1 = new SortedList<Thread *>(comp1);   /// demo!
    L2 = new SortedList<Thread *>(comp2);
    L3 = new List<Thread *>;
    toBeDestroyed = NULL;
}
// implement!
void Scheduler::updatePriority() // aging
{
    ListIterator<Thread *> *iter2 = new ListIterator<Thread *>(L2);
    ListIterator<Thread *> *iter3 = new ListIterator<Thread *>(L3);

    for (; !iter2->IsDone(); iter2->Next())
    {
        // DEBUG(dbgMp3, "---update in L2: " << kernel->stats->totalTicks - iter2->Item()->startWaitTime << " : " << kernel->stats->totalTicks << " : " << iter2->Item()->startWaitTime); /// debug!
        if (kernel->stats->totalTicks - iter2->Item()->startWaitTime >= 1500)
        {
            iter2->Item()->startWaitTime = kernel->stats->totalTicks;
            DEBUG(dbgMp3, "[C] Tick [" << kernel->stats->totalTicks << "] : Thread [" << iter2->Item()->getID() << "] changes its priority from [" << iter2->Item()->priority << "] to [" << iter2->Item()->priority + 10 << "]");
            iter2->Item()->priority += 10;
            if (iter2->Item()->priority >= 100)
            {
                DEBUG(dbgMp3, "[A] Tick [" << kernel->stats->totalTicks << "]: Thread [" << iter2->Item()->getID() << "] is inserted into queue L[1]");

                L1->Insert(iter2->Item());

                DEBUG(dbgMp3, "[B] Tick [" << kernel->stats->totalTicks << "] : Thread [" << iter2->Item()->getID() << "] is removed from queue L[2]");

                L2->Remove(iter2->Item());
                if (L2->IsEmpty())
                    break; /// debug!
            }
        }
    }
    for (; !iter3->IsDone(); iter3->Next())
    {
        // DEBUG(dbgMp3, "---update in L3: " << kernel->stats->totalTicks - iter3->Item()->startWaitTime << " : " << kernel->stats->totalTicks << " : " << iter3->Item()->startWaitTime); /// debug!
        if (kernel->stats->totalTicks - iter3->Item()->startWaitTime >= 1500)
        {
            iter3->Item()->startWaitTime = kernel->stats->totalTicks;
            DEBUG(dbgMp3, "[C] Tick [" << kernel->stats->totalTicks << "] : Thread [" << iter3->Item()->getID() << "] changes its priority from [" << iter3->Item()->priority << "] to [" << iter3->Item()->priority + 10 << "]");
            iter3->Item()->priority += 10;
            if (iter3->Item()->priority >= 50)
            {
                DEBUG(dbgMp3, "[A] Tick [" << kernel->stats->totalTicks << "]: Thread [" << iter3->Item()->getID() << "] is inserted into queue L[2]");

                L2->Insert(iter3->Item());

                DEBUG(dbgMp3, "[B] Tick [" << kernel->stats->totalTicks << "] : Thread [" << iter3->Item()->getID() << "] is removed from queue L[3]");

                L3->Remove(iter3->Item());
                if (L3->IsEmpty())
                    break; /// debug!
            }
        }
    }
}
//----------------------------------------------------------------------
// Scheduler::~Scheduler
// 	De-allocate the list of ready threads.
//----------------------------------------------------------------------

Scheduler::~Scheduler()
{
    delete L1;
    delete L2;
    delete L3;
    // delete readyList;
}

//----------------------------------------------------------------------
// Scheduler::ReadyToRun
// 	Mark a thread as ready, but not running.
//	Put it on the ready list, for later scheduling onto the CPU.
//
//	"thread" is the thread to be put on the ready list.
//----------------------------------------------------------------------

void Scheduler::ReadyToRun(Thread *thread)
{
    ASSERT(kernel->interrupt->getLevel() == IntOff);
    DEBUG(dbgThread, "Putting thread on ready list: " << thread->getName());
    // cout << "Putting thread on ready list: " << thread->getName() << endl ;
    thread->setStatus(READY);
    // readyList->Append(thread);
    thread->startWaitTime = kernel->stats->totalTicks;      // implement
    if (thread->priority >= 100)
    { // Insert into L1
        DEBUG(dbgMp3, "[A] Tick [" << kernel->stats->totalTicks << "]: Thread [" << thread->getID() << "] is inserted into queue L[1]");
        L1->Insert(thread);
    }
    else if (thread->priority >= 50)
    {
        DEBUG(dbgMp3, "[A] Tick [" << kernel->stats->totalTicks << "]: Thread [" << thread->getID() << "] is inserted into queue L[2]");
        L2->Insert(thread);
    }
    else
    {
        DEBUG(dbgMp3, "[A] Tick [" << kernel->stats->totalTicks << "]: Thread [" << thread->getID() << "] is inserted into queue L[3]");
        L3->Append(thread);
    }
}

//----------------------------------------------------------------------
// Scheduler::FindNextToRun
// 	Return the next thread to be scheduled onto the CPU.
//	If there are no ready threads, return NULL.
// Side effect:
//	Thread is removed from the ready list.
//----------------------------------------------------------------------

Thread *
Scheduler::FindNextToRun()
{
    ASSERT(kernel->interrupt->getLevel() == IntOff);

    /* if (readyList->IsEmpty())
    {
        return NULL;
    }
    else
    {
        return readyList->RemoveFront();
    } */

    // implement!
    if (!L1->IsEmpty())
    {
        DEBUG(dbgMp3, "[B] Tick [" << kernel->stats->totalTicks << "] : Thread [" << L1->Front()->getID() << "] is removed from queue L[1]");
        return L1->RemoveFront();
    }
    else if (!L2->IsEmpty())
    {
        DEBUG(dbgMp3, "[B] Tick [" << kernel->stats->totalTicks << "] : Thread [" << L2->Front()->getID() << "] is removed from queue L[2]");
        return L2->RemoveFront();
    }
    else if (!L3->IsEmpty())
    {
        DEBUG(dbgMp3, "[B] Tick [" << kernel->stats->totalTicks << "] : Thread [" << L3->Front()->getID() << "] is removed from queue L[3]");
        return L3->RemoveFront();
    }
    else
        return NULL;
}

//----------------------------------------------------------------------
// Scheduler::Run
// 	Dispatch the CPU to nextThread.  Save the state of the old thread,
//	and load the state of the new thread, by calling the machine
//	dependent context switch routine, SWITCH.
//
//      Note: we assume the state of the previously running thread has
//	already been changed from running to blocked or ready (depending).
// Side effect:
//	The global variable kernel->currentThread becomes nextThread.
//
//	"nextThread" is the thread to be put into the CPU.
//	"finishing" is set if the current thread is to be deleted
//		once we're no longer running on its stack
//		(when the next thread starts running)
//----------------------------------------------------------------------

void Scheduler::Run(Thread *nextThread, bool finishing)
{
    Thread *oldThread = kernel->currentThread;

    ASSERT(kernel->interrupt->getLevel() == IntOff);

    if (finishing)
    { // mark that we need to delete current thread
        ASSERT(toBeDestroyed == NULL);
        toBeDestroyed = oldThread;
    }

    if (oldThread->space != NULL)
    {                               // if this thread is a user program,
        oldThread->SaveUserState(); // save the user's CPU registers
        oldThread->space->SaveState();
    }

    oldThread->CheckOverflow(); // check if the old thread
                                // had an undetected stack overflow

    kernel->currentThread = nextThread; // switch to the next thread
    nextThread->setStatus(RUNNING);     // nextThread is now running

    DEBUG(dbgThread, "Switching from: " << oldThread->getName() << " to: " << nextThread->getName());

    // This is a machine-dependent assembly language routine defined
    // in switch.s.  You may have to think
    // a bit to figure out what happens after this, both from the point
    // of view of the thread and from the perspective of the "outside world".

    // TODO: DEBUG(dbgMp3, "");   // startRunTime should reset as 0;
    // Timothy DEBUG(dbgMp3, "[E] Tick [" << kernel->stats->totalTicks << "] : Thread [" << nextThread->getID() << "] is now selected for execution, thread [" << oldThread->getID() << "] is replaced, and it has executed [" << oldThread->burstTime << "] ticks");
    // oldThread->startRunTime = 0;                       // implement!
    nextThread->startRunTime = kernel->stats->totalTicks;
    // oldThread->burstTime = 0;
    // nextThread->startWaitTime = kernel->stats->totalTicks; /// debug

    SWITCH(oldThread, nextThread);
    // we're back, running oldThread

    // interrupts are off when we return from switch!
    ASSERT(kernel->interrupt->getLevel() == IntOff);

    DEBUG(dbgThread, "Now in thread: " << oldThread->getName());

    CheckToBeDestroyed(); // check if thread we were running
                          // before this one has finished
                          // and needs to be cleaned up

    if (oldThread->space != NULL)
    {                                  // if there is an address space
        oldThread->RestoreUserState(); // to restore, do it.
        oldThread->space->RestoreState();
    }
}

//----------------------------------------------------------------------
// Scheduler::CheckToBeDestroyed
// 	If the old thread gave up the processor because it was finishing,
// 	we need to delete its carcass.  Note we cannot delete the thread
// 	before now (for example, in Thread::Finish()), because up to this
// 	point, we were still running on the old thread's stack!
//----------------------------------------------------------------------

void Scheduler::CheckToBeDestroyed()
{
    if (toBeDestroyed != NULL)
    {
        delete toBeDestroyed;
        toBeDestroyed = NULL;
    }
}

//----------------------------------------------------------------------
// Scheduler::Print
// 	Print the scheduler state -- in other words, the contents of
//	the ready list.  For debugging.
//----------------------------------------------------------------------
void Scheduler::Print()
{
    cout << "Ready list contents:\n";
    readyList->Apply(ThreadPrint);
}
